job "job" {
  datacenters = ["dc1"]
  type        = "service"

  group "group" {
    task "task" {
      driver = "docker"

      config {
        image = "python:3-slim"

        args = [
          "python", "-m", "http.server",
          "--bind", "0.0.0.0",
          "--directory", "/",
          "80",
        ]

        volumes = [
          "/etc/localtime:/etc/localtime",
        ]
      }

      template {
        data = <<EOF
{{ env "NOMAD_ALLOC_ID" }}
EOF

        destination = "local/alloc-id.txt"
      }

      template {
        data = <<EOF
Secr3t
EOF

        destination = "secrets/secret.txt"
      }

      resources {
        network {
          port "http" {}
        }
      }
    }
  }
}

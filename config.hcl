bind_addr = "0.0.0.0"

client {
  network_interface = "eth0"
}

plugin "docker" {
  config {
    gc {
      image     = false
      container = false
    }

    volumes {
      enabled      = "true"
      selinuxlabel = "z"
    }
  }
}

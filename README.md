# Nomad SELinux labels on volumes

This project showcases an issue when using Nomad on a SELinux enforced system.

## Usage

```
$ vagrant up

$ vagrant ssh

$ sestatus
SELinux status:                 enabled
SELinuxfs mount:                /sys/fs/selinux
SELinux root directory:         /etc/selinux
Loaded policy name:             targeted
Current mode:                   enforcing
Mode from config file:          enforcing
Policy MLS status:              enabled
Policy deny_unknown status:     allowed
Memory protection checking:     actual (secure)
Max kernel policy version:      31

$ nomad agent -dev -config=config.hcl

$ nomad job run task.nomad
```

## Issue

SELinux is enforcing, Docker enables SELinux support.

```json
{
  "selinux-enabled": true
}
```

Nomad sets a default `selinuxlabel` so `/secrets` and `/local` are readable.

```hcl
plugin "docker" {
  config {
    volumes {
      enabled      = "true"
      selinuxlabel = "z"
    }
  }
}
```

Mount `/etc/localtime` into the container, which cannot be relabeled.

```hcl
        volumes = [
          "/etc/localtime:/etc/localtime"
        ]
```

## Fix

https://github.com/hashicorp/nomad/pull/7094

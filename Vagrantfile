# -*- mode: ruby -*-
# vi: set ft=ruby :

ENV['VAGRANT_DEFAULT_PROVIDER'] = 'libvirt'

NOMAD_VERSION = '0.12.5'

Vagrant.configure("2") do |config|

  config.vm.define "node" do |config|
    config.vm.hostname = "node"
    config.vm.box = "centos/8"
    config.vm.box_check_update = false
    config.vm.provider :libvirt do |v|
      v.memory = 1024
    end
  end

  config.vm.provision "shell",
                      privileged: false,
                      inline: <<-EOF
set -xe

if [ ! -e "/etc/docker/daemon.json" ]
then
  sudo dnf update -y --nobest

  sudo dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo
  sudo dnf install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm

  sudo dnf install -y --nobest \
    docker-ce \
    unzip

  sudo usermod -a -G docker $USER

  sudo mkdir -p /etc/docker
  echo '{"selinux-enabled": true}' | sudo tee /etc/docker/daemon.json

  sudo systemctl enable docker
  sudo systemctl start docker
fi

if [ ! -e "/usr/local/bin/nomad" ]
then
  curl -o nomad.zip https://releases.hashicorp.com/nomad/#{NOMAD_VERSION}/nomad_#{NOMAD_VERSION}_linux_amd64.zip
  sudo unzip nomad.zip nomad -d /usr/local/bin/
  rm nomad.zip
fi

EOF

  config.vm.provision "file", source: "config.hcl", destination: "$HOME/"
  config.vm.provision "file", source: "task.nomad", destination: "$HOME/"

end
